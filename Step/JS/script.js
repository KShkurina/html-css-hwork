
function app() {
    const buttons = document.querySelectorAll('.servis')
    const cards = document.querySelectorAll('.service-wrapper')
    function filter(category, items) {
        items.forEach((item) => {

            const isItemfiltred = !item.classList.contains(category)
            if (isItemfiltred) {
                item.classList.add('hide')
            }
            else { item.classList.remove('hide') }
        })
    }
    buttons.forEach((button) => {

        button.addEventListener('click', () => {
            const currentCategory = button.dataset.filter
            filter(currentCategory, cards)
            for (let btn of buttons) {
                btn.classList.remove('servis-hover:before', 'servis-hover')
            }
            button.classList.add('servis-hover:before', 'servis-hover')
        })
    })
}
app()

///GOSSIP


let index = 0;
const prev = document.getElementById('btnPrev')
const next = document.getElementById('btnNext')
const slides = document.querySelectorAll('.slide')
const dots = document.querySelectorAll('.dot')


const activeSlide = n => {
    for (let slide of slides) {
        slide.classList.remove('slide-active')
    }
    slides[n].classList.add('slide-active', 'go')

}
const activeDot = n => {
    for (let dot of dots) {
        dot.classList.remove('dot-active')
    }
    dots[n].classList.add('dot-active')
}

let prepareCurrentSlide = ind => {
    activeSlide(ind)
    activeDot(ind)
}

const nextSlide = () => {
    if (index === slides.length - 1) {
        index = 0;
        prepareCurrentSlide(index)

    }
    else {
        index++;
        prepareCurrentSlide(index)
    }
}
const prevSlide = () => {
    if (index === 0) {
        index = slides.length - 1;
        prepareCurrentSlide(index)
    }
    else {
        index--;
        prepareCurrentSlide(index)
    }
}

dots.forEach((item, indexDot) => {
    item.addEventListener('click', () => {
        index = indexDot
        prepareCurrentSlide(index)
    })
})

prev.addEventListener('click', prevSlide)
next.addEventListener('click', nextSlide)


//Our Amazing Work
let amButtons = document.querySelectorAll('.amazing')
let amazingList = document.querySelector('.amazing-list')
let amazingButtonAdd = document.querySelector('.amazing-button')
let currentFilter = 'All'
let maxImg = 12;
let arrToShow = []


let arr = [
    {
        src: './IMG/ourAmazingWork/graphic-design1.jpg',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design2.jpg',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design3.jpg',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design4.jpg',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design5.jpg',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design6.jpg',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design7.jpg',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design8.jpg',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design9.jpg',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design10.jpg',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design11.jpg',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design12.jpg',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design13.png',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design14.png',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design15.png',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design16.png',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design17.png',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design18.png',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design19.png',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design20.png',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design21.png',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design22.png',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design23.png',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design24.png',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design25.jpg',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design26.jpg',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design27.jpg',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design28.jpg',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design29.jpg',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design30.jpg',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design31.jpg',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design32.jpg',
        text: 'Wordpress',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design33.jpg',
        text: 'Web Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design34.jpg',
        text: 'Graphic Design',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design35.jpg',
        text: 'Landing Pages',
    },
    {
        src: './IMG/ourAmazingWork/graphic-design36.jpg',
        text: 'Wordpress',
    },
]


let amazingImgWrapper = document.querySelector('.amazing-img-wrapper')
function renderImg() {
    amazingImgWrapper.innerHTML = null

    arrToShow = arr.slice([0], [maxImg])
    for (let i = 0; i < arrToShow.length; i++) {
        if (amazingImgWrapper.children.length < maxImg && (currentFilter === arrToShow[i].text || currentFilter === "All")) {
            amazingImgWrapper.innerHTML += `
                <div style="background-image: url(${arrToShow[i].src})" class="amazing-img am-img-active">
                    <div class="amazing-hover-wrapper">
                        <a href="#" class="amaising-hover-btn">
                            <img src="./IMG/buttonLeft.png" alt="">
                            <img src="./IMG/buttonRight.png" alt="">
                        </a>
                        <a class="amaising-hover-head" href="#">creative design</a>
                        <span class="amaising-hover-sort">${arrToShow[i].text}</span>
                    </div>
                </div>
            `
        }
    }
}

renderImg();



amazingButtonAdd.addEventListener('click', () => {

    maxImg += 12
    if (maxImg >= 36) {
        amazingButtonAdd.remove()
    }
    renderImg()
})

amazingList.addEventListener('click', (event) => {

    for (let btn of amButtons) {
        btn.classList.remove('am-choice:before', 'am-choice')
    }
    currentFilter = event.target.innerText
    event.target.classList.add('am-choice:before', 'am-choice')
    renderImg()
});

