let input = document.querySelector('.input')
let inputWrapper = document.querySelector('.input-wrapper')
let p = document.createElement('p')
p.innerText = `Please enter correct price`
let span = document.createElement('span')
let buttonCross = document.createElement('button')

input.addEventListener('focus', function () {
    input.classList.remove('wrong')
    input.classList.add('active')
    p.remove()



})
input.addEventListener('blur', function () {

    if (input.value === "") {
        input.classList.remove('active')
        buttonCross.remove()
        span.remove()


        return
    }
    if (input.value < 0) {
        input.classList.add('wrong')
        inputWrapper.after(p)

        return
    }

    span.remove()
    buttonCross.remove()
    input.classList.remove('active')
    let value = input.value

    span.innerText = `Текущая цена: ${value}`
    document.body.insertBefore(span, inputWrapper)

    buttonCross.classList.add('button-cross')
    buttonCross.innerHTML = 'X'
    document.body.insertBefore(buttonCross, inputWrapper,)
    input.classList.add('valid')
    buttonCross.addEventListener('click', () => {
        buttonCross.remove()
        span.remove()
        input.value = ('')
    })
})

