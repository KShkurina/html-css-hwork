let img = document.querySelectorAll('.image')
let wrapper = document.querySelector('.images-wrapper')
let buttonBreak = document.querySelector('.break')
let buttonContinue = document.querySelector('.continue')


for (let i = 1; i < img.length; i++) {
    img[i].remove()

}
let counter = 1;
function show() {
    for (let i = 0; i < img.length; i++) {
        img[i].remove()
    }
    wrapper.append(img[counter])
    counter++
    if (counter === img.length) {
        counter = 0
    }
}
let interval = setInterval(show, 1000)

buttonBreak.addEventListener('click', () => clearInterval(interval))
buttonContinue.addEventListener('click', () => {
    interval = setInterval(show, 1000)

})
