
// if (!localStorage.theme) localStorage.theme = "orange"
// document.body.classList.add(localStorage.theme)
// themeBtn.onclick = () => {
//     document.body.classList.toggle("niceTheme")
//     localStorage.theme = document.body.className || "orange"
// }
// let them = document.querySelectorAll('.main-link-hover');



// DOMContentLoaded событие когда прогрузится страница
// let ls = localStorage.getItem('class') class это ключ и я его сама придумываю и в 23 строчне изначально

let btn = document.querySelector('.btn');
document.addEventListener('DOMContentLoaded', () => {
    let ls = localStorage.getItem('class')

    if (ls) {
        document.body.classList.add(ls)
    }

    btn.addEventListener('click', () => {

        if (!document.body.classList.contains('niceTheme')) {
            document.body.classList.add('niceTheme')
            localStorage.setItem('class', 'niceTheme')
        }
        else {
            document.body.classList.remove('niceTheme')
            localStorage.removeItem('class')
        }


    })
})