$(".button").click(() => {
    $(".top-rated").fadeToggle(5000)
})
$(".btn-posts").click(() => {
    $(".section-popular-posts").slideToggle(10)
})
$(".hot").click(() => {
    $(".hot-news-content").slideToggle(10000)
    $(".hn-title").slideToggle(5000)
})
$(".clients").click(() => {
    $(".popular-clients").fadeToggle(5000)
})

let goHome = document.querySelector(".goHome")
let header = document.querySelector("header")

document.addEventListener("scroll", () => {
    if (window.scrollY > (window.innerHeight - header.clientHeight)) {
        goHome.style.display = "block"
    }
    else {
        goHome.style.display = "none"
    }

})

