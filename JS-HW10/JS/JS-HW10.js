let input1 = document.querySelector('.input1')
let input2 = document.querySelector('.input2')
let passwordIcon1Active = document.querySelector("#passwordIcon1Active")
let passwordIcon2Active = document.querySelector("#passwordIcon2Active")
let passwordIcon1 = document.querySelector("#passwordIcon1")
let passwordIcon2 = document.querySelector("#passwordIcon2")
let button = document.querySelector('.btn')
let error = document.createElement('p')
error.classList.add('error')
error.innerHTML = "Нужно ввести одинаковые значения"

input1.addEventListener('keypress', () => error.remove())

input2.addEventListener('keypress', () => error.remove())


passwordIcon1Active.addEventListener('click', () => {

    input1.type = "text"
    passwordIcon1Active.classList.add('invisible')
    passwordIcon1.classList.remove('invisible')


})
passwordIcon2Active.addEventListener('click', () => {
    input2.type = "text"
    passwordIcon2Active.classList.add('invisible')
    passwordIcon2.classList.remove('invisible')



})
passwordIcon1.addEventListener('click', () => {
    input1.type = "password"
    passwordIcon1Active.classList.remove('invisible')
    passwordIcon1.classList.add('invisible')

})
passwordIcon2.addEventListener('click', () => {
    input2.type = "password"
    passwordIcon2Active.classList.remove('invisible')
    passwordIcon2.classList.add('invisible')
})

button.addEventListener("click", (event) => {
    event.preventDefault()

    if (input1.value === input2.value) {
        alert('You are welcome')
    }
    else {
        input2.after(error)

    }
})
