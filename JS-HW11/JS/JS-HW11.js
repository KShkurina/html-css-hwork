let buttons = document.querySelectorAll('.btn');
document.addEventListener('keypress', (event) => {

    for (let i = 0; i < buttons.length; i++) {
        buttons[i].classList.remove('active')
        if (event.key.toLowerCase() === buttons[i].innerHTML.toLowerCase()) {
            buttons[i].classList.add('active')
        }
    }
})