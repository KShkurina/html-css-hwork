
function app() {
    const buttons = document.querySelectorAll('.servis')
    const cards = document.querySelectorAll('.service-wrapper')
    function filter(category, items) {
        items.forEach((item) => {

            const isItemfiltred = !item.classList.contains(category)
            if (isItemfiltred) {
                item.classList.add('hide')
            }
            else { item.classList.remove('hide') }
        })
    }
    buttons.forEach((button) => {

        button.addEventListener('click', () => {
            const currentCategory = button.dataset.filter
            filter(currentCategory, cards)
            for (let btn of buttons) {
                btn.classList.remove('servis-hover:before', 'servis-hover')
            }
            button.classList.add('servis-hover:before', 'servis-hover')

        })
    })
}
app()

///GOSSIP


let index = 0;
const prev = document.getElementById('btnPrev')
const next = document.getElementById('btnNext')
const slides = document.querySelectorAll('.slide')
const dots = document.querySelectorAll('.dot')
console.log(slides)
// console.log(slides)




const activeSlide = n => {
    console.log(n)
    for (let slide of slides) {
        slide.classList.remove('slide-active')
    }
    slides[n].classList.add('slide-active', 'go')

}
const activeDot = n => {
    console.log(n)
    for (let dot of dots) {
        dot.classList.remove('dot-active')
    }
    dots[n].classList.add('dot-active')
}

let prepareCurrentSlide = ind => {
    activeSlide(ind)
    activeDot(ind)
}

const nextSlide = () => {
    if (index === slides.length - 1) {
        index = 0;
        prepareCurrentSlide(index)

    }
    else {
        index++;
        prepareCurrentSlide(index)
    }
}
const prevSlide = () => {
    if (index === 0) {
        index = slides.length - 1;
        prepareCurrentSlide(index)
    }
    else {
        index--;
        prepareCurrentSlide(index)
    }
}

dots.forEach((item, indexDot) => {
    item.addEventListener('click', () => {
        index = indexDot
        prepareCurrentSlide(index)
    })
})

prev.addEventListener('click', prevSlide)
next.addEventListener('click', nextSlide)


//Our Amazing Work


let imgs = document.querySelectorAll(".am-img-active")
let amazingImgWrapper = document.querySelector(".amazing-img-wrapper")
let allBtn = document.querySelector(".all-btn")
let webDesignBtn = document.querySelector(".web-design-btn")
let graphicDesignBtn = document.querySelector(".graphic-design-btn")
let landingPagesBtn = document.querySelector(".landing-pages-btn")
let wordpressBtn = document.querySelector(".wordpress-btn")
let amazingButtonAdd = document.querySelector('.amazing-button')

allBtn.addEventListener('click', () => {
    amazingImgWrapper.innerHTML = null
    imgs.forEach((element) => {
        amazingImgWrapper.append(element)
    })

})

webDesignBtn.addEventListener('click', () => {
    amazingImgWrapper.innerHTML = null
    imgs.forEach((element) => {
        if (element.classList.contains("web-design-img")) {
            amazingImgWrapper.append(element)
        }
    });
});

graphicDesignBtn.addEventListener('click', () => {
    amazingImgWrapper.innerHTML = null
    imgs.forEach((element) => {
        if (element.classList.contains("graphic-design-img")) {
            amazingImgWrapper.append(element)
        }
    });
});
landingPagesBtn.addEventListener('click', () => {
    amazingImgWrapper.innerHTML = null
    imgs.forEach((element) => {
        if (element.classList.contains("landing-pages-img"))
            amazingImgWrapper.append(element)
    })
})
wordpressBtn.addEventListener('click', () => {
    amazingImgWrapper.innerHTML = null
    imgs.forEach((element) => {
        if (element.classList.contains("wordpress-img")) {
            amazingImgWrapper.append(element)
        }
    })
})

amazingButtonAdd.addEventListener('click', () => {



    imgs.forEach((element) => {
        element.classList.add('imgAdd')

        // console.log(imgs);
    })

    amazingButtonAdd.remove()
})




